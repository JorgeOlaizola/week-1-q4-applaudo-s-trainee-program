const konami = 'applaudo'
const pressed = []

// KONAMI CODE

let modal = document.getElementById("myModal");

export function konamiCode(event){
    if(event.key === 'Escape') return modal.style.display = 'none';
    pressed.push(event.key.toLowerCase())
    pressed.splice(-konami.length - 1, pressed.length - konami.length)
    if(pressed.join('') === konami) {
        modal.style.display = "block";
    }
}

export function closeModal(event){
    modal.style.display = 'none';
}

export function closeWindow (event){
    if(event.target == modal){
        modal.style.display = 'none';
    }
}

// WELCOME FUNCTION

export function welcome(){
    document.getElementsByClassName('welcome')[0].style.opacity = 0
    setTimeout(function() {
        document.getElementById('form').style.opacity = 1
        document.getElementsByClassName('welcome')[0].className = 'hidden'
    }, 500)    
}    

// FORM

export function formValidator(event){
    event.preventDefault()

    const errors = document.getElementsByClassName('errorline')

    if(errors.length <= 0){

        // ------------- Sending data to the console

        const data = {
            name: document.getElementById('name').value,
            lastname: document.getElementById('lastname').value,
            email: document.getElementById('email').value,
            password: '*'.repeat(document.getElementById('password').value.length),
            phone: document.getElementById('phone').value,
            age: document.getElementById('rangeAge').value,
            website: document.getElementById('website').value,
            seniority: document.getElementById('seniority').value
        }
        console.log(data)

        // --------------- Closing Form
        
        let form = document.getElementById('form')
        let box = document.getElementById('box')

        // -- Removing previous box content
        while(box.childNodes.length > 0){
            box.removeChild(box.lastChild)
        }

        // -- Adding new content

        let p = document.createElement('p')
        let content = document.createTextNode('Thanks for applying to Applaudo! Your information is now in our database. We will contact you in case you match any position.')
        p.appendChild(content)
        box.appendChild(p)

        // -- Changing boxes
        form.style.opacity = 0
        box.className = 'welcome'
        setTimeout(function() {
            form.className = 'hidden'
            box.style.opacity = 1
        }, 500)
    }
    else{
        let errorsBox = document.getElementById('errors')
        let errorList = document.getElementById('errorList')

        // ---- Removing previous childs
        for(let i = 0; errorList.childNodes.length > 0; i++){
            errorList.removeChild(errorList.lastChild)
        }

        // ---- Setting errors list
        errorsBox.className = 'errors'
        for(let i = 0; i < errors.length; i++){
            let li = document.createElement('li')
            let content = document.createTextNode(errors[i].childNodes[0].nodeValue)
            li.appendChild(content)
            errorList.appendChild(li)
        }
    }
}

// EMAIL

const validEmail = /[\w._%+-]+@[\w.-]+\.[a-zA-Z]{2,4}/

export function emailValidator(event){
    let email = document.getElementById('email')
    let errorEmail = document.getElementById('errorEmail')

    if(!validEmail.test(event.target.value)){
        errorEmail.hasChildNodes() ? errorEmail.removeChild(errorEmail.lastChild) : null
        errorEmail.appendChild(document.createTextNode('Enter a valid email adress'))
        errorEmail.className = 'errorline'
        email.style.border = '1px solid red'
    }
    else{
        errorEmail.className = 'hidden'
        email.style.border = '1px solid black'
    }
}

// PHONE

export function phoneValidator(event){
    let phone = document.getElementById('phone')
    let errorPhone = document.getElementById('errorPhone')
    
    if(event.target.value.length < 8){
        errorPhone.hasChildNodes() ? errorPhone.removeChild(errorPhone.lastChild) : null
        errorPhone.appendChild(document.createTextNode('Your phone should have at list 8 numbers'))
        errorPhone.className = 'errorline'
        phone.style.border = '1px solid red'
    }
    else{
        errorPhone.className = 'hidden'
        phone.style.border = '1px solid black'
    }
}

// AGE

export function ageValue(event){
    let ageNumber = document.getElementById('ageNumber')
    
    ageNumber.hasChildNodes() ? ageNumber.removeChild(ageNumber.lastChild) : null
    ageNumber.appendChild(document.createTextNode(event.target.value + ' years'))
}


// PASSWORDS

export function passwordValidator(event){
    let errorPassword = document.getElementById('errorPassword')
    let password = document.getElementById('password')
    
    let validPassword = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/

    if(event.target.value.length < 8){
        errorPassword.hasChildNodes() ? errorPassword.removeChild(errorPassword.lastChild) : null
        errorPassword.appendChild(document.createTextNode('The password should be 8 characters long at list'))
        errorPassword.className = 'errorline'
        password.style.border = '1px solid red'
    }
    else if(!validPassword.test(password.value)){
        errorPassword.hasChildNodes() ? errorPassword.removeChild(errorPassword.lastChild) : null
        errorPassword.appendChild(document.createTextNode('The password must contain at least one upper case letter, one lower case letter, one number and one special character'))
        errorPassword.className = 'errorline'
        password.style.border = '1px solid red'
    }
    else{
        errorPassword.className = 'hidden'
        password.style.border = '1px solid black'
    }
}

export function matchPassword(event){
    let password = document.getElementById('password')
    let errorPassword2 = document.getElementById('errorPassword2')
    let password2 = document.getElementById('password2')

    if(event.target.value !== password.value){
        errorPassword2.hasChildNodes() ? errorPassword2.removeChild(errorPassword2.lastChild) : null
        errorPassword2.appendChild(document.createTextNode('The passwords should match'))
        errorPassword2.className = 'errorline'
        password2.style.border = '1px solid red'
    }
    else{
        errorPassword2.className = 'hidden'
        password2.style.border = '1px solid black'
    }
}

// NAME

let validName = /[^a-zA-Z]/

export function nameValidator(event){
    let name = document.getElementById('name')
    let errorName = document.getElementById('errorName')

    if(validName.test(event.target.value)){
        errorName.hasChildNodes() ? errorName.removeChild(errorName.lastChild) : null
        errorName.appendChild(document.createTextNode('You can only use letters in your name'))
        errorName.className = 'errorline'
        name.style.border = '1px solid red'
    }
    else{
        errorName.className = 'hidden'
        name.style.border = '1px solid black'
    }
}

// LASTNAME

export function lastnameValidator(event){
    let lastname = document.getElementById('lastname')
    let errorLastname = document.getElementById('errorLastname')

    if(validName.test(event.target.value)){
        errorLastname.hasChildNodes() ? errorLastname.removeChild(errorLastname.lastChild) : null
        errorLastname.appendChild(document.createTextNode('You can only use letters in your lastname'))
        errorLastname.className = 'errorline'
        lastname.style.border = '1px solid red'
    }
    else{
        errorLastname.className = 'hidden'
        lastname.style.border = '1px solid black'
    }
}

// WEBSITE

let validWebsite = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-])\/?$/

export function websiteValidator(event){
    let website = document.getElementById('website')
    let errorWebsite = document.getElementById('errorWebsite')

    if(!validWebsite.test(event.target.value)){
        errorWebsite.hasChildNodes() ? errorWebsite.removeChild(errorWebsite.lastChild) : null
        errorWebsite.appendChild(document.createTextNode('Enter a valid Website URL'))
        errorWebsite.className = 'errorline'
        website.style.border = '1px solid red'
    }
    else{
        errorWebsite.className = 'hidden'
        website.style.border = '1px solid black'
    }
}