import {
        konamiCode, 
        welcome, 
        formValidator, 
        phoneValidator, 
        ageValue, 
        emailValidator, 
        passwordValidator, 
        matchPassword, 
        nameValidator, 
        lastnameValidator, 
        websiteValidator,
        closeModal,
        closeWindow
        } from './utils.js'
// _____________________________________________________________________________________________________________________

// Konami Code

window.addEventListener('keyup', konamiCode)

// Konami Code --> Close modal

document.getElementById('closeModal').addEventListener('click', closeModal)
window.onclick = closeWindow

// Welcome function

document.getElementById('welcomeMsg').addEventListener('click', welcome)

// Form listener

document.getElementById('form').addEventListener('submit', formValidator)

//Password listener

document.getElementById('password').addEventListener('keyup', passwordValidator)

document.getElementById('password2').addEventListener('keyup', matchPassword)

//Phone listener

document.getElementById('phone').addEventListener('keyup', phoneValidator)

// Age listener

document.getElementById('rangeAge').addEventListener('change', ageValue)

// Email listener

document.getElementById('email').addEventListener('keyup', emailValidator)

// Name listener

document.getElementById('name').addEventListener('keyup', nameValidator)

// Lastname listener

document.getElementById('lastname').addEventListener('keyup', lastnameValidator)

// Website listener

document.getElementById('website').addEventListener('keyup', websiteValidator)


//----------------------------
